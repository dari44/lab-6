import java.util.Scanner;

public class Jackpot{
	public static void main(String[] args){		
		//Part 3
		/*
		Board mainBoard = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		*/
		//Bonus
		boolean playAgain = true;
		Scanner keyboard = new Scanner(System.in);
		int timesWon = 0;
		
		while (playAgain == true){
			Board mainBoard = new Board();
			boolean gameOver = false;
			int numOfTilesClosed = 0;
			gameOver = false;
			while (gameOver == false){
				System.out.println(mainBoard);
				if (mainBoard.playATurn() == true){
					gameOver = true;
				}
				else {
					numOfTilesClosed += 1;
					
				}
			}
			
			if (numOfTilesClosed >= 7){
				System.out.println("You have reached the jackpot and won!");
				timesWon++;
			}
			else{
				System.out.println("You lost!");
			}
			System.out.println("Do you wish to continue playing?");
			String userAnswer = keyboard.nextLine();
			
			if (userAnswer.equals("yes")){
				playAgain = true;
			}
			else{
				playAgain = false;
			}
		}
		System.out.println("You have won a total of " + timesWon + " times");
	}
}