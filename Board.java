public class Board{
	private Die firstDie;
	private Die secDie;
	private boolean[] tiles;
	
	 public Board(){
		firstDie = new Die();
		secDie = new Die();
		tiles = new boolean[12];
	}
	
	public String toString(){
		String toStringResult = "";
		
		for (int i = 0; i < tiles.length; i ++){
			if (tiles[i] == false){
				toStringResult += i + " ";
			}
			else{
				toStringResult += "X ";
			}
		}
		return toStringResult;
	}
	
	public boolean playATurn(){
		firstDie.roll();
		secDie.roll();
		System.out.println(firstDie);
		System.out.println(secDie);
		
		//calculate sum
		int sumOfDice = firstDie.getFaceValue() + secDie.getFaceValue();
		
		
		if (tiles[sumOfDice - 1] == false){
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice); 
			return false;
		}
		
		//1st die
		if (tiles[firstDie.getFaceValue()] == false){
			tiles[firstDie.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die one: " + firstDie.getFaceValue());
			return false;
		}
		
		//2nd die
		if (tiles[secDie.getFaceValue()] == false){
			tiles[secDie.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die two: " + secDie.getFaceValue());
			return false;
		}
		
		System.out.println("All the tiles for these values are already shut");
		return true;
	}
}